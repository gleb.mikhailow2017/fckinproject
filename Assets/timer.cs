﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timer : MonoBehaviour
{
    public float timel;
    public bool done = false;
    void Update()
    {
        if (timel <= 0) done = true;
        else { timel -= Time.deltaTime; done = false; }
    }
}
