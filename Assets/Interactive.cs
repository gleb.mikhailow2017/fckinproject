﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactive : MonoBehaviour
{
    GameManager GM;
    private void Start()
    {
        GM = FindObjectOfType<GameManager>();
    }
    public virtual void Opendoor()
    {
        GM.Nextlv();
    }
    public virtual void Active()
    {

    }
}
