﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour
{
    Animator OnOff;
    bool Open = false;
    private void Start()
    {
        OnOff = gameObject.GetComponent<Animator>();
    }
    public void Active()
    {
        Open = !Open;
        OnOff.SetBool("Open", Open);
    }
}
