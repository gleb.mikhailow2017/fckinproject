﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melle : MonoBehaviour
{
    public float timerl;
    public int damage;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col != null && col.gameObject.tag == "enemy")
        {
            col.GetComponent<Enemy>().HP -= damage;
        }
    }
}
