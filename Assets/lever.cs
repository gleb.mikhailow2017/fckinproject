﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lever : Interactive
{
    door door;
    Animator OnOff;
    bool IsOn = false;
    private void Start()
    {
        OnOff = gameObject.GetComponent<Animator>();
        door = GetComponentInChildren<door>();
    }
    public override void Active()
    {
        IsOn = !IsOn;
        OnOff.SetBool("On",IsOn);
        door.Active();
    }
}
