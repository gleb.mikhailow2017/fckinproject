﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerPl : MonoBehaviour
{
    public Canvas Invent;

    public GameObject weapon;
    public bool mlWpnd;
    public Text interact;
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Invent.enabled = !Invent.enabled;
        }

        if (weapon != null && weapon.tag == "melee") mlWpnd = true;
        else mlWpnd = false;
        if (Input.GetMouseButtonDown(1))
        {
            if (mlWpnd && gameObject.GetComponent<timer>().done)
            {
                weapon.SetActive(true);
                weapon.GetComponent<Animation>().Play();
                gameObject.GetComponent<timer>().timel = weapon.GetComponent<Melle>().timerl;
            }
        }
        if (!weapon.GetComponent<Animation>().isPlaying)
        {
            weapon.SetActive(false);
        }
    }
    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "interactive")
        {
            interact.text = "Interact";
            if (Input.GetKeyDown(KeyCode.E))
            {
                col.gameObject.GetComponent<Interactive>().Active();
            }
        }
        else
        {
            interact.text = "nope";
        }
        if (col.gameObject.tag == "portal")
        {
            interact.text = "Go";
            if (Input.GetKeyDown(KeyCode.E))
            {
                col.gameObject.GetComponent<Interactive>().Opendoor();
            }
        }
    }
}
