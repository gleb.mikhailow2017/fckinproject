﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Chest : Interactive
{
    Canvas can;
    private void Start()
    {
        can = GetComponentInChildren<Canvas>();
    }
    public void Active()
    {
        can.enabled = true;
    }
    private void Update()
    {
        if (can.enabled == true && Input.GetKeyDown(KeyCode.Escape))
        {
            can.enabled = false;
        }
    }
}
