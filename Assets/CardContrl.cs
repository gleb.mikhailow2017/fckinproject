﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardContrl : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public ControllerPl player;

    public Camera main;
    Vector3 offset;
    public Transform defPar;
    public void OnBeginDrag(PointerEventData eventData)
    {
        offset = transform.position- main.ScreenToWorldPoint(eventData.position);

        transform.SetParent(defPar.parent);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 NewPos = main.ScreenToWorldPoint(eventData.position);
        NewPos.z = -1f;
        transform.position = NewPos + offset;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(defPar);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if(defPar == GameObject.FindGameObjectWithTag("Arm"))
        {
            player.mlWpnd = true;
        }
        else
        {
            player.mlWpnd = false;
        }
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<ControllerPl>();
    }
}
