﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropPlace : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        CardContrl card = eventData.pointerDrag.GetComponent<CardContrl>();

        if (card)
        {
            card.defPar = transform;
        }
    }
}
